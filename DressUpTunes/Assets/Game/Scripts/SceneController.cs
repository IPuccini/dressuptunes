﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;

public class SceneController : MonoBehaviour
{
    [SerializeField]
    private float introTime = 1f;
    [SerializeField]
    private float introDelay = 1f;

    public RectTransform items;
    private Vector2 itemsOriginalPosition;

    public RectTransform logo;
    public Vector2 logoOriginalScale;
    public RectTransform exit;
    private Vector2 exitOriginalPosition;
    public RectTransform play;
    public Vector2 playOriginalScale;

    public GameObject character;
    public Vector3 originalCharacterScale;


    public void Awake()
    {
        itemsOriginalPosition = items.anchoredPosition;
        items.anchoredPosition = new Vector3(itemsOriginalPosition.x + 300, itemsOriginalPosition.y);

        logoOriginalScale = logo.localScale;
        logo.localScale = Vector2.zero;
        logo.DOScale(logoOriginalScale.x, .5f).SetEase(Ease.OutBack).SetDelay(.5f);

        playOriginalScale = play.localScale;
        play.localScale = Vector2.zero;
        play.DOScale(playOriginalScale.x, .5f).SetEase(Ease.OutBack).SetDelay(.8f);

        exitOriginalPosition = exit.anchoredPosition;
        exit.anchoredPosition = new Vector3(exitOriginalPosition.x + 100, exitOriginalPosition.y);
        exit.DOAnchorPosX(exitOriginalPosition.x, .5f).SetDelay(1.3f);

        originalCharacterScale = character.transform.localScale;
        character.transform.localScale = Vector3.zero;
    }

    public void Initialize()
    {
        logo.DOScale(0, .2f).SetEase(Ease.InBack);
        play.DOScale(0, .2f).SetEase(Ease.InBack);


        items.anchoredPosition = new Vector3(itemsOriginalPosition.x + 300, itemsOriginalPosition.y);
        items.DOAnchorPosX(itemsOriginalPosition.x, introTime).SetDelay(introDelay).SetEase(Ease.OutCubic);

        character.transform.DOScale(originalCharacterScale.x, introTime).SetDelay(introDelay).SetEase(Ease.OutBack);
    }

    public void Play()
    {
        Initialize();
    }


    public void Exit()
    {
        Application.Quit();
    }
}
