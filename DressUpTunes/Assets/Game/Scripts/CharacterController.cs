﻿using UnityEngine;
using System.Collections;
using Spine.Unity;
using UnityEngine.Audio;
using System.Collections.Generic;

public class CharacterController : MonoBehaviour
{
    private SkeletonAnimation skeletonAnimation;
    public AudioMixer audioMixer;
    public AudioMixerSnapshot hatSnapshot;
    public AudioMixerSnapshot headSnapshot;
    public AudioMixerSnapshot bodySnapshot;
    public AudioMixerSnapshot backgroundSnapshot;
    public AudioMixerSnapshot allSnapshot;
    public AudioSource sync;


    public AudioSource audioHat;
    public int hatIndex = 0;

    public AudioSource audioHead;
    public int headIndex = 0;

    public AudioSource audioBody;
    public int bodyIndex = 0;

    public AudioSource audioBackground;
    public int backgroundIndex = 0;


    public List<string> hats;
    public List<string> head;
    public List<string> body;
    public List<string> lArm;
    public List<string> rArm;
    public List<string> lLeg;
    public List<string> rLeg;

    public SpriteRenderer backgroundSprite;
    public List<string> background;
    public List<Sprite> backgroundSprites;

    public List<string> dance;


    public ParticleSystem dustParticles;
    bool init = true;
    public void Start()
    {
        skeletonAnimation = GetComponent<SkeletonAnimation>();
        GetAttachmentNames();

        ChangeHat();
        ChangeBody();
        ChangeHead();

        init = false;
    }

    private void GetAttachmentNames()
    {
        hats = new List<string>();
        hats = GetAttachmentAtSlot("hat");

        head = new List<string>();
        head = GetAttachmentAtSlot("head");

        body = new List<string>();
        body = GetAttachmentAtSlot("body");

        lArm = new List<string>();
        lArm = GetAttachmentAtSlot("l-arm");

        rArm = new List<string>();
        rArm = GetAttachmentAtSlot("r-arm");

        lLeg = new List<string>();
        lLeg = GetAttachmentAtSlot("l-leg");

        rLeg = new List<string>();
        rLeg = GetAttachmentAtSlot("r-leg");
    }

    private List<string> GetAttachmentAtSlot(string slot)
    {
        List<Spine.Attachment> slotAttachment = new List<Spine.Attachment>();
        List<string> attachmentNames = new List<string>();

        int index = skeletonAnimation.Skeleton.FindSlotIndex(slot);
        skeletonAnimation.Skeleton.Data.DefaultSkin.FindAttachmentsForSlot(index, slotAttachment);
        for(int i = 0; i < slotAttachment.Count; i++)
        {
            attachmentNames.Add(slotAttachment[i].Name);
            Debug.Log(attachmentNames[i]);
        }

        return attachmentNames;
    }


    public void ChangeSlot(string slot, string newAttachment)
    {
        skeletonAnimation.skeleton.FindSlot(slot).Data.attachmentName = newAttachment;
        skeletonAnimation.skeleton.FindSlot(slot).SetToSetupPose();

        if(!init)
        {
            dustParticles.Play();
        }
        
        skeletonAnimation.AnimationName = dance[Random.Range(0, dance.Count)];
    }

    public void ChangeHat()
    {

        hatIndex++;
        if(hatIndex >= hats.Count)
            hatIndex = 0;
        Debug.Log("index: " + hatIndex);
        ChangeSlot("hat", hats[hatIndex]);


        hatSnapshot.TransitionTo(.1f);

        audioHat.Stop();
        audioHat.clip = (AudioClip) Resources.Load("Musics/Hat/" + hats[hatIndex], typeof(AudioClip));
        audioHat.Play();
        audioHat.timeSamples = sync.timeSamples;


        Invoke("SetAllSnapshot", .1f);
    }

    public void SetAllSnapshot()
    {
        allSnapshot.TransitionTo(.1f);
    }
    

    public void ChangeHead()
    {
        headIndex++;
        if(headIndex >= head.Count)
            headIndex = 0;
        Debug.Log("index: " + headIndex);
        ChangeSlot("head", head[headIndex]);

        headSnapshot.TransitionTo(.1f);
        audioHead.Stop();
        audioHead.clip = (AudioClip)Resources.Load("Musics/Head/" + head[headIndex], typeof(AudioClip));
        audioHead.Play();
        audioHead.timeSamples = sync.timeSamples;
        Invoke("SetAllSnapshot", .1f);
    }


    public void ChangeBody()
    {
        bodyIndex++;
        if(bodyIndex >= body.Count)
            bodyIndex = 0;
        Debug.Log("index: " + bodyIndex);
        ChangeSlot("body", body[bodyIndex]);
        ChangeSlot("l-leg", lLeg[bodyIndex]);
        ChangeSlot("r-leg", rLeg[bodyIndex]);
        ChangeSlot("l-arm", lArm[bodyIndex]);
        ChangeSlot("r-arm", rArm[bodyIndex]);

        bodySnapshot.TransitionTo(.1f);
        audioBody.Stop();
        audioBody.clip = (AudioClip)Resources.Load("Musics/Body/" + body[bodyIndex], typeof(AudioClip));
        audioBody.Play();
        audioBody.timeSamples = sync.timeSamples;
        Invoke("SetAllSnapshot", .1f);
    }

    public void ChangeBackground()
    {
        backgroundIndex++;
        if(backgroundIndex >= background.Count)
            backgroundIndex = 0;
        Debug.Log("index: " + backgroundIndex);
        //ChangeSlot("background", background[backgroundIndex]);


        backgroundSprite.sprite = backgroundSprites[Random.Range(0, backgroundSprites.Count)];

        backgroundSnapshot.TransitionTo(.1f);
        audioBackground.Stop();
        audioBackground.clip = (AudioClip)Resources.Load("Musics/Background/" + background[backgroundIndex], typeof(AudioClip));
        audioBackground.Play();
        audioBackground.timeSamples = sync.timeSamples;
        Invoke("SetAllSnapshot", .1f);
    }

    public void RandomDance()
    {

    }
}
